window.AudioContext = window.AudioContext || window.webkitAudioContext;
var context = new AudioContext();

window.URL = window.URL || window.webkitURL;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

var soundClips = document.getElementById("recordSection");

var constraints = {audio: true};
var chunks = [];

//var output = document.getElementById("output");
//function writeToScreen(message) {
//    output.innerHTML += message;
//}

var onFail = function (e) {
    console.log('Rejected!', e);
};

var onSuccess = function (s) {

//    recorder = new WebAudioRecorder(mediaStreamSource);
    var recorder = new MediaRecorder(s);
    recorder.start();

    recorder.onstop = function (e) {
        console.log("data available after MediaRecorder.stop() called.");
        var clipName = prompt('Enter a name for your sound clip');
        var clipContainer = document.createElement('article');
        var clipLabel = document.createElement('a');
        var newLine= document.createElement('br');
        var audio = document.createElement('audio');
        var deleteButton = document.createElement('button');

        clipContainer.classList.add('clip');
        audio.setAttribute('controls', '');
        deleteButton.innerHTML = "Delete";
        clipLabel.innerHTML = clipName;
        

        audio.controls = true;
        var blob = new Blob(chunks, {'type': 'audio/x-wav'});
        chunks = [];
        var audioURL = URL.createObjectURL(blob);
        audio.src = audioURL;
        
        clipLabel.href = audioURL;
        clipLabel.download = clipName+'&nbsp;&nbsp;&nbsp;';
        
        console.log("recorder stopped");
        
        clipContainer.appendChild(newLine);
        clipContainer.appendChild(audio);
        clipContainer.appendChild(newLine);
        clipContainer.appendChild(clipLabel);
        clipContainer.appendChild(deleteButton);
        soundClips.appendChild(clipContainer);
            
        deleteButton.onclick = function (e) {
            evtTgt = e.target;
            evtTgt.parentNode.parentNode.removeChild(evtTgt.parentNode);
        };
    };

    recorder.ondataavailable = function (e) {
        chunks.push(e.data);
    };

    var btnDisc = document.getElementById("btnstop");
    btnDisc.addEventListener("click", stopRecording, false);
    function stopRecording() {
        context.close();
        //Stop recording
        recorder.stop();
    }
};

function startRecording() {
    chunks = [];
    if (navigator.getUserMedia) {
        navigator.getUserMedia({audio: true}, onSuccess, onFail);
    } else {
        console.log('navigator.getUserMedia not present');
    }
}


//audio/x-wav


